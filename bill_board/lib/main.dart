import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.greenAccent[400],
      ),
      home: MyHomePage(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var fab = Icons.add;
  void changeFab() {
    setState(() {
      if (fab == Icons.add)
        fab = Icons.cancel;
      else
        fab = Icons.add;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        color: Colors.black38,
        child: ListView(
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.only(right: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  IconButton(
                      icon: Icon(Icons.menu),
                      onPressed: (){},
                      color: Colors.grey[800]),
                  Center(
                      child: Text(
                    "Home",
                    style: TextStyle(fontStyle: FontStyle.italic, fontSize: 22),
                  )),
                  IconButton(
                    icon: Icon(Icons.account_circle),
                    iconSize: 40.0,
                    onPressed: (){},
                    color: Colors.grey[800],
                  ),
                ],
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.4,
            ),
            Container(
              padding: EdgeInsets.all(10),
              height: MediaQuery.of(context).size.height * 0.35,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: Container(
                  color: Colors.greenAccent[400],
                  child: ListView(
                    padding: EdgeInsets.all(10),
                    children: <Widget>[
                      Center(
                          child: Text(
                        "Month",
                        style: TextStyle(
                            fontSize: 20,
                            color: Colors.white,
                            fontStyle: FontStyle.italic),
                      )),
                      ListTile(
                        title: Text(
                          "Stocks Purchased",
                          style: TextStyle(fontSize: 20, color: Colors.white),
                        ),
                        subtitle: Text("10000",
                            style:
                                TextStyle(fontSize: 15, color: Colors.white)),
                        trailing: IconButton(
                            icon: Icon(Icons.open_in_new), onPressed: null),
                      ),
                      ListTile(
                        title: Text("Stocks sold",
                            style:
                                TextStyle(fontSize: 20, color: Colors.white)),
                        subtitle: Text("100000",
                            style:
                                TextStyle(fontSize: 15, color: Colors.white)),
                        trailing: IconButton(
                            icon: Icon(Icons.open_in_new), onPressed: null),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width * 0.9,
              height: MediaQuery.of(context).size.height * 0.15,
              padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(10),
                    width: MediaQuery.of(context).size.width*0.3,
                        child: IconButton(
                          icon: Icon(Icons.announcement),
                          iconSize: 30,
                          onPressed: (){},
                          focusColor: Colors.purple[600],
                          color: Colors.purple[600],
                          highlightColor: Colors.greenAccent[400],
                          hoverColor: Colors.purple[600],
                        ),
                  ),
                  FloatingActionButton(
                    onPressed: () {
                      changeFab();
                    },
                    child: Icon(fab),
                    backgroundColor: Colors.greenAccent[400],
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    width: MediaQuery.of(context).size.width*0.3,
                        child: IconButton(
                          icon: Icon(Icons.chat_bubble_outline),
                          iconSize: 30,
                          onPressed: (){},
                          focusColor: Colors.purple[600],
                          color: Colors.purple[600],
                          highlightColor: Colors.greenAccent[400],
                          hoverColor: Colors.purple[600],
                        ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
